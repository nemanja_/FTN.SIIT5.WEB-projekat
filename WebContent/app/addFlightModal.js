Vue.component('addFlightModal', {
	data() {
		return {
			flightNum: '',
			startDestination: '',
			endDestination: '',
			price: '',
			planeModel: '',
			firstClassSeats: '',
			businessClassSeats: '',
			economicClassSeats: '',
			date: '',
			flightClass: '',
			destinations: [],
			flightClasses: []
		}
	},
	template: `
	<transition name="modal">
	    <div class="modal-mask">
	        <div class="modal-wrapper">
	            <div class="modal-container">
	                <div class="row">
	                    <div class="col-md-12 centered">
	                        <div class="modal-header text-center centered">
	                            <h3> NEW FLIGHT </h3>
	                        </div>
	                    </div>
	                </div>
	                <div class="row modal-body">
	                    <div class="col-md-12 centered "> </div>
	                    <form>
	                        <div class="form-group row">
	                            <label class="col-sm-2 col-form-label">Flight number</label>
	                            <div class="col-sm-10">
	                                <input type="text" v-model="flightNum" class="form-control" placeholder="Flight number" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Starting destination</label>
	                            <div class="col-sm-10">
	                                <select v-model="startDestination" class="form-control" required>
	                                	<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Ending destination</label>
	                            <div class="col-sm-10">
	                                <select v-model="endDestination" class="form-control" required>
	                                	<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Price</label>
	                            <div class="col-sm-10">
	                                <input type="number" step=any min="0" v-model="price" class="form-control" placeholder="Price" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Airplane</label>
	                            <div class="col-sm-4">
	                                <input type="text" v-model="planeModel" class="form-control" id="phone" placeholder="Airplane model" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">First seats</label>
	                            <div class="col-sm-1">
	                                <input type="number" step=1 min="0" v-model="firstClassSeats" class="form-control" placeholder="#" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">Business seats</label>
	                            <div class="col-sm-1">
	                                <input type="number" step=1 min="0" v-model="businessClassSeats" class="form-control" placeholder="#" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">Economic seats</label>
	                            <div class="col-sm-1">
	                                <input type="number" step=1 min="0" v-model="economicClassSeats" class="form-control" placeholder="#" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-2 col-form-label">Date</label>
	                            <div class="col-sm-10">
	                                <input type="date" v-model="date" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-2 col-form-label">Flight class</label>
	                            <div class="col-sm-10">
	                                <select v-model="flightClass"  class="form-control">
	                                	<option v-for="flightClass in flightClasses" v-bind:value="flightClass" required> {{flightClass}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <div class="col-sm-11">
	                                <button type="submit" class="btn btn-primary" @click="add">Add</button>
	                            </div>
	                            <div class="col-sm-1">
	                                <button class="modal-default-button btn btn-danger" @click="$emit('close')">Close</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</transition>
	`,
	methods: {
		add(e) {
			e.preventDefault()
			const airplane = {
				'model': this.planeModel,
				'numFirstClassSeats': this.firstClassSeats,
				'numBusinessClassSeats': this.businessClassSeats,
				'numEconomicClassSeats': this.economicClassSeats
			}
			const flight = {
				'flightNum': this.flightNum,
				'startDestination': this.startDestination,
				'endDestination': this.endDestination,
				'price': this.price,
				'reservations': null,
				'airplane': airplane,
				'flightDate': new Date(this.date),
				'flightClass': this.flightClass
			}
			this.$emit('addFlight', flight)
		}
	},
	mounted() {
		axios.get('rest/destinations_svc/getDestinations')
		.then(response => this.destinations = response.data.destinations)
		.catch(err => console.log(err))
		axios.get('rest/flights_svc/getFlightClasses')
		.then(response => this.flightClasses = response.data)
		.catch(err => console.log(err))
	},
})