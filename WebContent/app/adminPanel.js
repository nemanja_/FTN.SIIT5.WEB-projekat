Vue.component("admin-panel", {
	data() {
		return {
			currentView: FlightsView,
			isActive: 1,
		}
	},
	template: `
	<div class="container-fluid">
		<nav class="navbar navbar-default">
		<div>
	    <div class="navbar-header">
	      	<a class="navbar-brand" href="#">ADMIN PANEL</a>
	    </div>
		    <ul class="nav navbar-nav">
		      <li v-bind:class="{ active: isActive == 1 }"><a href="#" v-on:click.prevent="flights_comp()">Flights</a></li>
		      <li v-bind:class="{ active: isActive == 2 }"><a href="#" v-on:click.prevent="destinations_comp()">Destinations</a></li>
		      <li v-bind:class="{ active: isActive == 3 }"><a href="#" v-on:click.prevent="users_comp()">Users</a></li>
		      <li v-bind:class="{ active: isActive == 4 }"><a href="#" v-on:click.prevent="userinfo_comp()">Edit profile</a></li>
		      <li><a href="#" v-on:click.prevent="logOut()">Log out</a></li>
		    </ul>
		</div>
		</nav>
		<component class="component-view" :is="currentView"></component>
	</div>
	`,
	methods: {
		flights_comp: function() {
			this.isActive = 1
			this.currentView = FlightsView
		},
		destinations_comp : function() {
			this.isActive = 2
			this.currentView = DestinationsView
		},
		users_comp : function() {
			this.isActive = 3
			this.currentView = UsersView
		},
		userinfo_comp : function() {
			this.isActive = 4
			this.currentView = UserInfoView
		},
		logOut: function() {
			axios.post('rest/login_svc/logout')
			.then(response => router.push("/"))
			.catch(err => console.log(err))
		}
	},
	created() {
		axios.get('rest/login_svc/checkLoginAdmin')
		.then(response => function(){
			if (response.data == false) {
				router.push("/")
			}
		}())
		.catch(err => console.log(err))
	},
	mounted() {
	},
});