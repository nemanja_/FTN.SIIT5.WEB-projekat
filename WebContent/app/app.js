const AdminPanel = { template: '<admin-panel></admin-panel>'}
const UserPanel = { template: '<user-panel></user-panel>'}
const LoginPanel = { template: '<login-panel></login-panel>'}
const RegistrationPanel = { template: '<registration-panel></registration-panel>'}
const StartPanel = { template: '<start-panel></start-panel>'}
const UsersView = {template: '<users-view></users-view>'}
const FlightsView = {template: '<flights-view></flights-view>'}
const DestinationsView = {template: '<destinations-view></destinations-view>'}
const UserInfoView = {template: '<userinfo-view></userinfo-view>'}
const AddFlightModal = {template: '<addFlight-modal></addFlight-modal>'}
const EditFlightModal = {template: '<editFlight-modal></editFlight-modal>'}
const AddDestinationModal = {template: '<addDestination-modal></addDestination-modal>'}
const EditDestinationModal = {template: '<editDestination-modal></editDestination-modal>'}
const FilterFlights = {template: '<filter-flights></filter-flights>'}
const SearchFlights = {template: '<search-flights></search-flights>'}
const ReservationsView = {template: '<reservations-view></reservations-view>'}
const ReserveView = {template: '<reserve-view></reserve-view>'}
const ReserveFlightModal = {template: '<reserveFlight-modal></reserveFlight-modal>'}
const GMapModal = {template: '<gmap-modal></gmap-modal>'}

const router = new VueRouter({
	mode: 'hash',
	routes: [
		{ path: '/', component: StartPanel},
		{ path: '/register', component: RegistrationPanel},
		{ path: '/admin', component: AdminPanel},
		{ path: '/user', component: UserPanel}
	]
})

var app = new Vue({
	router,
	el: '#app'
})