Vue.component("destinations-view", {
	template: `
	<div>
		<div class="row">
			<div class="col-md-1"> </div>
	        <div class="col-md-10">
	            <table class="table">
	                <thead class="thead-dark">
	                	<th scope="col"></th>
	                    <th scope="col">Destination name</th>
	                    <th scope="col">Destination country</th>
	                    <th scope="col">Airport name</th>
	                    <th scope="col">Airport code</th>
	                    <th scope="col">Active</th>
	                    <th scope="col">Edit</th>
	                </thead>
	                <tbody>
						<tr v-for="(destination, key) in destinations" v-bind:style= "[!destination.active ? {'background-color': '#808080'} : {}]">
							<td>
								<img class="centered mx-auto text-center" v-on:click="fileUpload(destination)" style="margin-bottom:50px;" v-bind:src="'data/assets/' + destination.picture" height="120" width="120">
								<input type="file" accept="image/*," v-on:change="updatePicture($event)" id="pictureInput" style="display: none">
							</td>
							<td>{{destination.name}}</td>
						    <td>{{destination.country}}</td>
						    <td>{{destination.airport.name}}</td>
						    <td>{{destination.airport.code}}</td>
						    <td><input type="checkbox" v-model="destination.active" v-on:change="changeActivity(destination)"></td>
						    <td> <button type="button" class="btn btn-warning" v-bind='destination' @click="fshowModalEdit(key)">Edit</button> </td>
						    <editDestination-modal v-show="showModalEdit[key]" v-bind:destinationOriginal="destination" @close="fcloseModalEdit(key)" @editDestination='editDestination'></editDestination-modal>
	    				</tr
	    				<tr>
	    					<td scope="col"> <button type="button" class="btn btn-primary" @click="showModalAdd = true">Add destination</button> </td>
	    				</tr>
	                </tbody>
	            </table>
	        </div>
	        <div class="col-md-1"> </div>
	        <addDestination-modal v-if="showModalAdd" @close="showModalAdd = false" @addDestination='addDestination'></addDestination-modal>
		</div>
	</div>
	`,
	data() {
		return {			
			destinations: [],
			showModalAdd: false,
			showModalEdit: [],
			selected: null,
			imageAsset: {
				file: null,
				extension: ""
			}
		}
	},
	methods: {
		addDestination(destination) {
			axios.post('rest/destinations_svc/addDestination', destination)
			.then(response => this.destinations = [...this.destinations, response.data])
			.catch(err => console.log(err))
		},
		changeActivity(destination) {
			axios.post('rest/destinations_svc/archiveDestination', destination)
		},
		editDestination(destination) {
			axios.post('rest/destinations_svc/changeDestination', destination)
			.then(response => this.destinations = response.data.destinations)
			.catch(err => console.log(err))
		},
		fshowModalEdit(key) {
			this.$set(this.showModalEdit, key, true)
		},
		fcloseModalEdit(key) {
			this.$set(this.showModalEdit, key, false)
		},
		fileUpload(destination) {
			this.selected = destination
			$("#pictureInput").click();
		},
		updatePicture(event) {
			var files = event.target.files || event.dataTransfer.files
			if (!files.length)
				return
				
			var file = files[0]
			var ext = files[0].name.split(".").pop()
			var headers = {
					"Content-Type": "image/*",
					"Image-Extension": ext,
					"Destination": this.selected.airport.code
			}
			axios.post('rest/destinations_svc/uploadPicture', file, {headers: headers})
			.then(response => axios.get('rest/destinations_svc/getDestinations')
					.then(response => this.destinations = response.data.destinations)
					.catch(err => console.log(err)))
			.catch(err => console.log(err))
		}
	},
	mounted() {
		axios.get('rest/destinations_svc/getDestinations')
		.then(response => {
			this.destinations = response.data.destinations
			for (var i = 0; i < this.destinations.length; i++)
				this.showModalEdit.push(false)
			})
		.catch(err => console.log(err))
	},
});