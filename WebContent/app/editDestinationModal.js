Vue.component('editDestinationModal', {
	props: ['destinationOriginal'],
	data() {
		return {
			destination: jQuery.extend(true, {}, this.destinationOriginal),
		}
	},
	template: `
	<transition name="modal">
	    <div class="modal-mask">
	        <div class="modal-wrapper">
	            <div class="modal-container">
	                <div class="row">
	                    <div class="col-md-12 centered">
	                        <div class="modal-header text-center centered">
	                            <h3> NEW DESTINATION </h3>
	                        </div>
	                    </div>
	                </div>
	                <div class="row modal-body">
	                    <div class="col-md-12 centered "> </div>
	                    <form>
	                        <div class="form-group row">
	                            <label class="col-sm-1 col-form-label">Name</label>
	                            <div class="col-sm-11">
	                                <input type="text" v-model="destination.name" class="form-control" placeholder="Name" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-1 col-form-label">Country</label>
	                            <div class="col-sm-11">
	                                <input type="text" v-model="destination.country" class="form-control" placeholder="Price" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-1 col-form-label">Airport</label>
	                            <div class="col-sm-2">
	                                <input type="text" v-model="destination.airport.name" class="form-control" id="phone" placeholder="Airport name" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">Airport code</label>
	                            <div class="col-sm-2">
	                                <input type="text" v-model="destination.airport.code" class="form-control" id="phone" placeholder="Airport code" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">Latitude</label>
	                            <div class="col-sm-2">
	                                <input type="number" step="any" v-model="destination.airport.coordinate.latitude" class="form-control" placeholder="#" required>
	                            </div>
	                            <label  class="col-sm-1 col-form-label">Longitude</label>
	                            <div class="col-sm-2">
	                                <input type="number" step="any" v-model="destination.airport.coordinate.longitude" class="form-control" placeholder="#" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <div class="col-sm-11">
	                                <button type="submit" class="btn btn-primary" @click="edit">Edit</button>
	                            </div>
	                            <div class="col-sm-1">
	                                <button class="modal-default-button btn btn-danger" @click="$emit('close')">Close</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</transition>
	`,
	methods: {
		edit(e) {
			this.$emit('editDestination', this.destination)
		},
		closeDialog(e) {
			e.preventDefault()
			this.destination = jQuery.extend(true, {}, this.destinationOriginal)
			this.$emit('close')
		}
	},
	mounted() {
	},
})