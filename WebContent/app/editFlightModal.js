Vue.component('editFlightModal', {
	props: ['flightOriginal'],
	data() {
		return {
			flight: jQuery.extend(true, {}, this.flightOriginal),
			startDestination: '',
			endDestination: '',
			flightClass: '',
			destinations: [],
			flightClasses: []
		}
	},
	template: `
	<transition name="modal">
	    <div class="modal-mask">
	        <div class="modal-wrapper">
	            <div class="modal-container">
	                <div class="row">
	                    <div class="col-md-12 centered">
	                        <div class="modal-header text-center centered">
	                            <h3> EDIT FLIGHT </h3>
	                        </div>
	                    </div>
	                </div>
	                <div class="row modal-body">
	                    <div class="col-md-12 centered "> </div>
	                    <form>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Starting destination</label>
	                            <div class="col-sm-10">
	                                <select v-model="flight.startDestination" required>
	                                	<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Ending destination</label>
	                            <div class="col-sm-10">
	                                <select v-model="flight.endDestination" required>
	                                	<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Price</label>
	                            <div class="col-sm-10">
	                                <input type="number" min="0" v-model="flight.price" class="form-control" placeholder="Price" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-2 col-form-label">Date</label>
	                            <div class="col-sm-10">
	                                <input type="date" v-model="flight.flightDate" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <label class="col-sm-2 col-form-label">Flight class</label>
	                            <div class="col-sm-10">
	                                <select v-model="flight.flightClass">
	                                	<option v-for="flightClass in flightClasses" v-bind:value="flightClass" required> {{flightClass}} </option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <div class="col-sm-11">
	                                <button type="submit" class="btn btn-primary" @click="edit">Edit</button>
	                            </div>
	                            <div class="col-sm-1">
	                                <button class="modal-default-button btn btn-danger" @click="closeDialog">Close</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</transition>
	`,
	methods: {
		edit(e) {
			if (!(typeof this.flight.date === 'string' || this.flight.date instanceof String)) {
				this.flight.date = this.flightOriginal.date
			} else {
				this.flight.date = new Date(this.flight.date)
			}
			this.$emit('editFlight', this.flight)
		},
		closeDialog(e) {
			e.preventDefault()
			this.flight = jQuery.extend(true, {}, this.flightOriginal)
			this.$emit('close')
		}
	},
	mounted() {
		axios.get('rest/destinations_svc/getDestinations')
		.then(response => this.destinations = response.data.destinations)
		.catch(err => console.log(err))
		axios.get('rest/flights_svc/getFlightClasses')
		.then(response => this.flightClasses = response.data)
		.catch(err => console.log(err))
	},
})