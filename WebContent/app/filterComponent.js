Vue.component('filterFlights', {
	props: ['flights'],
	data() {
		return {
			option: null,
			flightClasses: null,
			flightClass: null,
			flightNum: null
		}
	},
	template: `
	<form>
		<div class="form-row">
			<div class="col-md-1">
				<label col-md-1 col-form-label>FILTER</label>
			</div>
			<div class="col-md-2">
				<select v-model="option" class="form-control" v-on:change='filter'>
					<option value="none">No filter</option>
					<option value="latest">Latest</option>
					<option value="flightClass">Flight class</option>
					<option value="flightNumber">Flight number</option>
				</select>
			</div>
			<div class="col-md-9">
				<select v-if="option =='flightClass'" v-model="flightClass" class="form-control" v-on:change='filter'>
					<option v-for="flightClass in flightClasses" v-bind:value="flightClass" required> {{flightClass}} </option>
	            </select>
				<input type="text" v-model="flightNum" class="form-control" v-if="option == 'flightNumber'" v-on:input='filter' v-on:change='filter'>
			</div class="col">
		</div>
	</form>
	`,
	methods: {
		filter() {
			var retVal = []
			switch (this.option) {
				case 'latest': {
					var currentDate = new Date()
					var minDate = Number.MAX_VALUE;
					
					var dates = []
					for (key in this.flights)
						dates.push(this.flights[key].flightDate)
					dates.sort(function(a, b){return b-a})
					
					for (key in dates) {
						var diff = dates[key] - currentDate
						if (diff > 0 && diff < minDate) {
							minDate = dates[key]
						}
					}
					
					for (key in this.flights) {
						if (this.flights[key].flightDate == minDate) {
							retVal.push(key)
						}
					}
					break;
				}
				case 'flightClass': {
					if (this.flightClass != null) {
						for (key in this.flights) {
							if (this.flights[key].flightClass == this.flightClass)
								retVal.push(key)
						}
					} else {
						return
					}
					break;
				}
				case 'flightNumber': {
					if (this.flightNum != null) {
						for (key in this.flights) {
							if (this.flights[key].flightNum.toLowerCase().search(this.flightNum.toLowerCase()) != -1)
								retVal.push(key)
						}
					} else {
						return
					}
					break;
				}
				case 'none': {
					for (key in this.flights)
							retVal.push(key)
					break;
				}
			}
			this.$emit('filter', retVal)
		}
	},
	mounted() {
		axios.get('rest/flights_svc/getFlightClasses')
		.then(response => this.flightClasses = response.data)
		.catch(err => console.log(err))
	},
})