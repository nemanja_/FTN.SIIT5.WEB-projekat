Vue.component("flights-view", {
	template: `
	<div>
		<div class="row" id="searchRows">
			<div class="col-md-10 col-md-offset-1">
				<search-flights v-bind:flights="flights" @search="search"></search-flights>
			</div>
		</div>
		<div class="row" id="filterRows">
			<div class="col-md-10 col-md-offset-1">
				<filter-flights v-bind:flights="flights" @filter="filter"></filter-flights>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"> </div>
	        <div class="col-md-10">
	            <table class="table">
	                <thead class="thead-dark">
	                    <th scope="col">Flight number</th>
	                    <th scope="col">Starting destination</th>
	                    <th scope="col">Ending destination</th>
	                    <th scope="col">Price</th>
	                    <th scope="col">Airplane model</th>
	                    <th scope="col">Flight date</th>
	                    <th scope="col">Flight class</th>
	                </thead>
	                <tbody>
						<tr v-for="(flight, key) in flights" v-show="showFlightsSearch[key] && showFlightsFilter[key]">
							<td>{{flight.flightNum}}</td>
						    <td @click="displayMap(flight.startDestination.airport.coordinate)">{{flight.startDestination.name}}</td>
						    <td @click="displayMap(flight.endDestination.airport.coordinate)">{{flight.endDestination.name}}</td>
						    <td>{{flight.price}}</td>
						    <td>{{flight.airplane.model}}</td>
						    <td>{{flight.flightDate | dateFormat('DD.MM.YYYY')}}</td>
						    <td>{{flight.flightClass}}</td>
						    <td> <button type="button" class="btn btn-warning" v-bind='flight' @click="fshowModalEdit(key)"> Edit </button> </td>
						    <td> <button type="button" class="btn btn-danger" v-if='flight.reservations.reservations.length == 0' @click="deleteFlight(flight)"> Delete </button> </td>
						    <editFlight-modal v-show="showModalEdit[key]" v-bind:flightOriginal="flight" @close="fcloseModalEdit(key)" @editFlight='editFlight'></editFlight-modal>
	    				</tr>
	                </tbody>
	            </table>
	            <button type="button" class="btn btn-primary" @click="showModalAdd = true"> Add flight </button>
	        </div>
	        <div class="col-md-1"> </div>
		</div>
		<addFlight-modal v-if="showModalAdd" @close="showModalAdd = false" @addFlight='addFlight'></addFlight-modal>
		<gmap-modal v-if="showGMapModal" @close="showGMapModal = false" v-bind:coordinate="coordinate"></gmap-modal>
	</div>
	`,
	data() {
		return {			
			flights: [],
			showModalAdd: false,
			showModalEdit: [],
			showFlightsSearch: [],
			showFlightsFilter: [],
			showGMapModal: false,
			coordinate: null
		}
	},
	methods: {
		editFlight(flight) {
			axios.post('rest/flights_svc/editFlight', flight)
			.then(response => this.flights = response.data.flights)
			.catch(err => console.log(err))
		},
		deleteFlight(flight) {
			axios.post('rest/flights_svc/removeFlight', flight)
			.then(response => this.flights = response.data.flights)
			.catch(err => console.log(err))
		},
		addFlight(flight) {
			this.showModalEdit.push(false)
			this.showFlightsSearch.push(true)
			this.showFlightsFilter.push(true)
			axios.post('rest/flights_svc/addFlight', flight)
			.then(response => {
				console.log(response.data)
				if (response.data != null)
					this.flights = [...this.flights, response.data]
				else
					alert("Flight could not added")})
			.catch(err => console.log(err))
		},
		fshowModalEdit(key) {
			this.$set(this.showModalEdit, key, true)
		},
		fcloseModalEdit(key) {
			this.$set(this.showModalEdit, key, false)
		},
		search(data) {
			for (key in this.flights) {
				this.$set(this.showFlightsSearch, key, false)
			}
			for (key in data) {
				this.$set(this.showFlightsSearch, data[key], true)
			}
		},
		filter(data) {
			for (key in this.flights) {
				this.$set(this.showFlightsFilter, key, false)
			}
			for (key in data) {
				this.$set(this.showFlightsFilter, data[key], true)
			}
		},
		displayMap(coordinate) {
			this.coordinate = coordinate
			this.showGMapModal = true
		}
	},
	mounted() {
		axios.get('rest/flights_svc/getFlights')
		.then(response => {
			this.flights = response.data.flights
			for (var i = 0; i < this.flights.length; i++) {
				this.showModalEdit.push(false)
				this.showFlightsSearch.push(true)
				this.showFlightsFilter.push(true)
			}
			})
		.catch(err => console.log(err))
	},
	filters: {
    	dateFormat: function (value, format) {
    		var parsed = moment(value);
    		return parsed.format(format);
    	}
	}
});