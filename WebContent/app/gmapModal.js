Vue.component('gmapModal', {
	template: `
		<transition name="modal">
	    <div class="modal-mask">
	        <div class="modal-wrapper">
	            <div class="modal-container">
	                <div id="googleMap" style="width:100%;height:400px;">
	                </div>
	                <button class="modal-default-button btn btn-danger" @click="$emit('close')">Close</button>
	            </div>
	        </div>
	    </div>
	</transition>
	`,
	props: ['coordinate'],
	mounted() {
		var map = new google.maps.Map(
				document.getElementById("googleMap"), {
					center: new google.maps.LatLng(this.coordinate.latitude, this.coordinate.longitude),
					zoom: 8
		})
	},
})