Vue.component("login-panel", {
	data() {
		return {
			vue_username: '',
			vue_password: ''
		}
	},
	template: `
	<div>
		<div class="row">
			<div class="col-md-12 text-center">
				<h1> LOGIN </h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 centered">
				<form id="login_form" @submit="login">
					<div class="form-group row centered">
						<label class="col-md-2 col-form-label">Username</label>
						<div class="col-md-10">
							<input type="text" class="form-control" v-model="vue_username" name="login_usename" id="username" required>
						</div>
					</div>
					<div class="form-group row centered">
						<label class="col-md-2 col-form-label">Password</label>
						<div class="col-md-10">
							<input type="password" class="form-control" v-model="vue_password" name="login_password" id="password" required>
						</div>
					</div>
					<div class="form-group row centered">
						<div class="col-md-12">
							<input type="submit" name="btn_login" value="Login">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	`,
	methods: {
		login(e) {
			e.preventDefault();
			const log_username = this.vue_username
			const log_password = this.vue_password
			axios.post('rest/login_svc/login', {
				'username': log_username,
				'password': log_password
			})
			.then(response => function(){
				if (response.data == "ADMIN") {
					router.push("/admin")
				} else if (response.data == "USER") {
					router.push("/user")
				} else {
					alert("Invalid login info")
				}
			}())
			.catch(err => console.log(err))
		},
		register(e) {
			e.preventDefault()
			router.push("/register")
		}
	},
	mounted() {
	}
});