Vue.component("registration-panel", {
	data() {
		return {
			vue_username: '',
			vue_password: '',
			vue_name: '',
			vue_surname: '',
			vue_phone: '',
			vue_email: '',
		}
	},
	template: `
	<div>
		<div class="row">
			<div class="col-md-12 text-center">
				<h1> REGISTER </h1>
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 centered justify-content-center">
				<form id="register_form" @submit="register">
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Username</label>
						<div class="col-md-9">
							<input type="text" class="form-control" v-model="vue_username" name="reg_usename" id="regusername" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Password</label>
						<div class="col-md-9">
							<input type="password" class="form-control" v-model="vue_password" name="reg_password" id="regpassword" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Email</label>
						<div class="col-md-9">
							<input type="email" class="form-control" v-model="vue_email" name="reg_email" id="regemail" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Name</label>
						<div class="col-md-9">
							<input type="text" class="form-control" v-model="vue_name" name="reg_name" id="regname" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Surname</label>
						<div class="col-md-9">
							<input type="text" class="form-control" v-model="vue_surname" name="reg_surname" id="regsurname" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Phone</label>
						<div class="col-md-9">
							<input type="text" class="form-control" v-model="vue_phone" name="reg_phone" id="regphone" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<input type="submit" name="btn_register" value="Register">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	`,
	methods: {
		register(e) {
			e.preventDefault();
			const reg_username = this.vue_username
			const reg_password = this.vue_password
			const reg_name = this.vue_name
			const reg_surname = this.vue_surname
			const reg_phone = this.vue_phone
			const reg_email = this.vue_email	
			axios.post('rest/user_svc/registerUser', {
				'username': reg_username,
				'password': reg_password,
				'name': reg_name,
				'surname': reg_surname,
				'phoneNum': reg_phone,
				'email': reg_email,
				'active': true
			})
			.then(response => function(){
				if (response.data == true) {
					router.push("/user")
				} else {
					alert("Username already taken")
				}
			}())
			.catch(err => console.log(err))
		},
	},
	mounted() {
	},
});