Vue.component("reservations-view", {
	template: `
	<div>
		<div class="row">
			<div class="col-md-1"> </div>
	        <div class="col-md-10">
	            <table class="table">
	                <thead class="thead-dark">
	                    <th scope="col">Reservation code</th>
	                    <th scope="col">Date</th>
	                    <th scope="col">Seat class</th>
	                    <th scope="col">Tickets reserved</th>
	                    <th scope="col">Cancel</th>
	                </thead>
	                <tbody>
						<tr v-for="reservation in reservations">
							<td>{{reservation.id}}</td>
						    <td>{{reservation.date | dateFormat('DD.MM.YYYY')}}</td>
						    <td>{{reservation.seatClass}}</td>
						    <td>{{reservation.passengerNum}}</td>
						    <td> <button type="button" class="btn btn-error" @click="cancel(reservation)">Cancel</button> </td>
	    				</tr>
	                </tbody>
	            </table>
	        </div>
	        <div class="col-md-1"> </div>
		</div>
	</div>
	`,
	data() {
		return {			
			reservations: [],
		}
	},
	methods: {
		cancel(reservation) {
			axios.post('rest/reservations_svc/cancelReservation', reservation)
			.then(response => {
				if (response.data == false) {
					alert("Reservation could not be deleted")
				} else {
					axios.get('rest/reservations_svc/getReservations')
					.then(response => this.reservations = response.data.reservations)
					.catch(err => console.log(err))
				}
			})
			.catch(err => console.log(err))
		},
	},
	mounted() {
		axios.get('rest/reservations_svc/getReservations')
		.then(response => this.reservations = response.data.reservations)
		.catch(err => console.log(err))
	},
	filters: {
    	dateFormat: function (value, format) {
    		var parsed = moment(value);
    		return parsed.format(format);
    	}
	}
});