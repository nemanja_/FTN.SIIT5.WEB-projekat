Vue.component('reserveFlightModal', {
	props: ['flight'],
	data() {
		return {
			seatClasses : null,
			seatClass : null,
			numOfSeats : null,
		}
	},
	template: `
	<transition name="modal">
	    <div class="modal-mask">
	        <div class="modal-wrapper">
	            <div class="modal-container">
	                <div class="row">
	                    <div class="col-md-12 centered">
	                        <div class="modal-header text-center centered">
	                            <h3> RESERVE TICKET </h3>
	                        </div>
	                    </div>
	                </div>
	                <div class="row modal-body">
	                    <div class="col-md-12 centered "> </div>
	                    <form>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Seat class</label>
								<div class="col-sm-10">
									<select v-model="seatClass">
										<option v-for="seatClass in seatClasses" v-bind:value="seatClass" required> {{seatClass}} </option>
									</select>
								</div>
							</div>
							<div class="form-group row">
	                            <label  class="col-sm-2 col-form-label">Number of seats</label>
	                            <div class="col-sm-10">
	                                <input type="number" min="0" step="1" v-model="numOfSeats" class="form-control" placeholder="#" required>
	                            </div>
	                        </div>
	                        <div class="form-group row">
	                            <div class="col-sm-11">
	                                <button type="submit" class="btn btn-primary" @click="reserve">Reserve</button>
	                            </div>
	                            <div class="col-sm-1">
	                                <button class="modal-default-button btn btn-danger" @click="closeDialog">Close</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</transition>
	`,
	methods: {
		reserve(e) {
			e.preventDefault()
			const reservation = {
				'id': null,
				'user': null,
				'date': null,
				'seatClass': this.seatClass,
				'passengerNum': this.numOfSeats
			}
			const reservationInfo = {
				'reservation': reservation,
				'flight': this.flight
			}
			axios.post('rest/reservations_svc/addReservation', reservationInfo)
			.then(response => {
				if (response.data == false) {
					alert("Reservation could not be added")
				}
			})
			.catch(err => console.log(err))
			this.$emit('close')
		},
		closeDialog(e) {
			e.preventDefault()
			this.$emit('close')
		}
	},
	mounted() {
		axios.get('rest/flights_svc/getSeatClasses')
		.then(response => this.seatClasses = response.data)
		.catch(err => console.log(err))
	},
})