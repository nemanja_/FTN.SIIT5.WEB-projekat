Vue.component("reserve-view", {
	template: `
	<div>
	    <div class="row">
	        <div class="col-md-12">
	            <form>
	                <div class="form-group row">
						<label class="col-md-1 col-form-label">Starting destination</label>
						<div class="col-md-3">
							<select v-model="startDestination" class="form-control" required>
								<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
							</select>
						</div>
						<label class="col-md-1 col-form-label">Ending destination</label>
						<div class="col-md-3">
							<select v-model="endDestination" class="form-control" required>
								<option v-for="destination in destinations" v-bind:value="destination"> {{destination.name}} </option>
							</select>
						</div>
						<label class="col-md-1 col-form-label">Date</label>
						<div class="col-md-2">
							<input type="date" v-model="date" required>
						</div>
						<div class="col-md-1">
							<button type="submit" class="btn btn-primary" @click="lookup">Lookup</button>
						</div>
					</div>
	            </form>
	        </div>
	    </div>
	    <div class="row" id="filterRows">
			<div class="col-md-10 col-md-offset-1">
				<filter-flights v-bind:flights="flights" @filter="filter"></filter-flights>
			</div>
		</div>
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
	            <table class="table">
	                <thead class="thead-dark">
	                    <th scope="col-md-2">Flight number</th>
	                    <th scope="col-md-2">From</th>
	                    <th scope="col-md-2">To</th>
	                    <th scope="col-md-2">Price</th>
	                    <th scope="col-md-2">Flight class</th>
	                    <th scope="col-md-2">Reserve</th>
	                </thead>
	                <tbody>
	                	<tr v-for="(flight, key) in flights" v-show="showFlightsLookup[key] && showFlightsFilter[key]">
							<td>{{flight.flightNum}}</td>
						    <td @click="displayMap(flight.startDestination.airport.coordinate)">
						    	<div class="row">
						    		<div class="col centered align-items-center mx-auto text-center">
						    			<img class="centered mx-auto text-center" v-if="flight.startDestination.picture" v-bind:src="'data/assets/' + flight.startDestination.picture" height="120" width="120">
						    		</div>
						    	</div>
						    	<div class="row">
						    		<div class="col">
										{{flight.startDestination.airport.name}}, {{flight.startDestination.name}}, {{flight.startDestination.country}}
									</div>
						    	</div> 
						    </td>
						    <td @click="displayMap(flight.endDestination.airport.coordinate)">
						    	<div class="row">
						    		<div class="col centered align-items-center mx-auto text-center">
						    			<img class="centered mx-auto text-center" v-if="flight.endDestination.picture" v-bind:src="'data/assets/' + flight.endDestination.picture" height="120" width="120">
						    		</div>
						    	</div>
						    	<div class="row">
						    		<div class="col">
						    			{{flight.endDestination.airport.name}}, {{flight.endDestination.name}}, {{flight.endDestination.country}}
						    		</div>
						    	</div> 
						    </td><td>{{flight.price}}</td>
						    <td>{{flight.airplane.model}}</td>
						    <td>{{flight.flightClass}}</td>
						    <td> <button type="button" class="btn btn-warning" v-bind='flight' @click="ftoggleModalReserve(key)">Reserve</button></td>
						    <reserveFlight-modal v-show="showModalReserve[key]" v-bind:flight="flight" @close="ftoggleModalReserve(key)" @reserve="ftoggleModalReserve(key)"></reserveFlight-modal>
	    				</tr>
	                </tbody>
	            </table>
	            <gmap-modal v-if="showGMapModal" @close="showGMapModal = false" v-bind:coordinate="coordinate"></gmap-modal>
	        </div>
		</div>
	</div>
	`,
	data() {
		return {
			flights: [],
			destinations: [],
			startDestination: null,
			endDestination: [],
			date: null,
			showModalReserve: [],
			showFlightsFilter: [],
			showFlightsLookup: [],
			showGMapModal: false,
			coordinate: null
		}
	},
	methods: {
		lookup(e) {
			e.preventDefault()
			for (key in this.flights) {
				if (this.flights[key].startDestination.name == this.startDestination.name &&
					this.flights[key].endDestination.name == this.endDestination.name &&
					this.flights[key].flightDate == (new Date(this.date)).getTime()) {
					this.$set(this.showFlightsLookup, key, true)
				} else {					
					this.$set(this.showFlightsLookup, key, false)
				}
			}
		},
		filter(data) {
			for (key in this.flights) {
				this.$set(this.showFlightsFilter, key, false)
			}
			for (key in data) {
				this.$set(this.showFlightsFilter, data[key], true)
			}
		},
		ftoggleModalReserve(key) {
			this.$set(this.showModalReserve, key, !(this.showModalReserve[key]))
		},
		displayMap(coordinate) {
			this.coordinate = coordinate
			this.showGMapModal = true
		}
	},
	mounted() {
		axios.get('rest/flights_svc/getUserFlights')
		.then(response => {
			this.flights = response.data.flights
			for (var i = 0; i < this.flights.length; i++) {
				this.showModalReserve.push(false)
				this.showFlightsFilter.push(true)
			}})
		.catch(err => console.log(err))
		axios.get('rest/destinations_svc/getDestinations')
		.then(response => this.destinations = response.data.destinations)
		.catch(err => console.log(err))
	},
	
});