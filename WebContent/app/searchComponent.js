Vue.component('searchFlights', {
	props: ['flights'],
	data() {
		return {
			option: null,
			destinations: null,
			startDestination: null,
			endDestination: null,
			bothDestinations: null,
			country: null
		}
	},
	template: `
	<form id="searchForm">
		<div class="form-row">
			<div class="col-md-1">
				<label col-md-1 col-form-label>SEARCH</label>
			</div>
			<div class="col-md-2">
				<select v-model="option" class="form-control" v-on:change='search'>
					<option value="none">None</option>
					<option value="startDestination">Starting destination</option>
					<option value="endDestination">Ending destination</option>
					<option value="bothDestinations">Both destinations</option>
					<option value="country">Country</option>
				</select>
			</div>
			<div class="col-md-9">
				<input type="text" v-model="startDestination" class="form-control" v-if="option == 'startDestination'" v-on:change='search' v-on:input='search'>
				<input type="text" v-model="endDestination" class="form-control" v-if="option == 'endDestination'" v-on:change='search' v-on:input='search'>
				<input type="text" v-model="bothDestinations" class="form-control" v-if="option == 'bothDestinations'" v-on:change='search' v-on:input='search'>
				<input type="text" v-model="country" class="form-control" v-if="option == 'country'" v-on:input='search' v-on:change='search'>
			</div class="col">
		</div>
	</form>
	`,
	methods: {
		search() {
			var retVal = []
			switch (this.option) {
				case 'startDestination': {
					if (this.startDestination == null)
						return
					for (key in this.flights) {
						if (this.flights[key].startDestination.name.toLowerCase().search(this.startDestination.toLowerCase()) != -1)
							retVal.push(key)
					}
					break;
				}
				case 'endDestination': {
					if (this.endDestination == null)
						return
					for (key in this.flights) {
						if (this.flights[key].endDestination.name.toLowerCase().search(this.endDestination.toLowerCase()) != -1)
							retVal.push(key)
					}
					break;
				}
				case 'bothDestinations': {
					if (this.bothDestinations == null)
						return
					for (key in this.flights) {
						if (this.flights[key].endDestination.name.toLowerCase().search(this.bothDestinations.toLowerCase()) != -1 ||
								this.flights[key].startDestination.name.toLowerCase().search(this.bothDestinations.toLowerCase()) != -1)
							retVal.push(key)
					}
					break;
				}
				case 'country': {
					if (this.country == null)
						return
					for (key in this.flights) {
						if (this.flights[key].startDestination.country.toLowerCase().search(this.country.toLowerCase()) != -1 ||
								this.flights[key].endDestination.country.toLowerCase().search(this.country.toLowerCase()) != -1)
							retVal.push(key)
					}
					break;
				}
				case 'none': {
					for (key in this.flights)
							retVal.push(key)
				}
			}
			this.$emit('search', retVal)
		}
	},
	mounted() {
		axios.get('rest/destinations_svc/getDestinations')
		.then(response => this.destinations = response.data.destinations)
		.catch(err => console.log(err))
	},
})