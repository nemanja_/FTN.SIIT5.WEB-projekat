Vue.component("start-panel", {
	template: `
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-4 centered">
				<login-panel></login-panel>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4 centered">
				<registration-panel></registration-panel>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	`,
	methods: {
	},
	mounted() {
	},
});