Vue.component("user-panel", {
	data() {
		return {
			currentView: ReserveView,
			isActive: 1,
		}
	},
	template: `
	<div class="container-fluid">
		<nav class="navbar navbar-default">
		<div>
	    <div class="navbar-header">
	      	<a class="navbar-brand">USER PANEL</a>
	    </div>
		    <ul class="nav navbar-nav">
		      <li v-bind:class="{ active: isActive == 1 }"><a href="#" v-on:click.prevent="reserve_comp()">Reserve</a></li>
		      <li v-bind:class="{ active: isActive == 2 }"><a href="#" v-on:click.prevent="reservations_comp()">Reservations</a></li>
		      <li v-bind:class="{ active: isActive == 3 }"><a href="#" v-on:click.prevent="userinfo_comp()">Edit profile</a></li>
		      <li v-bind:class="{ active: isActive == 4 }"><a href="#" v-on:click.prevent="logOut()">Log out</a></li>
		    </ul>
		</div>
		</nav>
		<component class="component-view" :is="currentView"></component>
	</div>
	`,
	methods: {
		reserve_comp : function() {
			this.isActive = 1
			this.currentView = ReserveView
		},
		reservations_comp : function() {
			this.isActive = 2
			this.currentView = ReservationsView
		},
		userinfo_comp : function() {
			this.isActive = 3
			this.currentView = UserInfoView
		},
		logOut: function() {
			this.isActive = 4
			axios.post('rest/login_svc/logout')
			.then(response => router.push("/"))
			.catch(err => console.log(err))
		}
	},
	created() {
		axios.get('rest/login_svc/checkLoginUser')
		.then(response => function(){
			if (response.data == false) {
				router.push("/")
			}
		}())
		.catch(err => console.log(err))
	},
	mounted() {
	},
});