Vue.component("userinfo-view", {
	data() {
		return {			
			user: {},
			backup: {},
			admin: null,
			imageAsset: {
				file: null,
				extension: ""
			}
		}
	},
	template: `
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4 centered align-items-center mx-auto text-center">
					<img class="centered mx-auto text-center" height="184" width="184" style="margin-bottom:50px;" v-on:click="fileUpload" v-if="user.picture" v-bind:src="'data/assets/' + user.picture">
					<input type="file" accept="image/*," @change="updatePicture" id="picture" style="display: none">
				</div>
				<div class="col-md-4"></div>
			</div>
			<div class="row">
				<div class="col-md-12 centered "> </div>
					<form>
					    <div class="form-group row">
					      <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
					      <div class="col-sm-10">
					        <input type="email" class="form-control" v-model="user.email" required>
					      </div>
					    </div>
					    <div class="form-group row">
					      <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
					      <div class="col-sm-10">
					        <input type="password" class="form-control" id="pass" v-model="user.password" required>
					      </div>
					    </div>
					    <div class="form-group row">
					        <label for="inputPassword3" class="col-sm-2 col-form-label">Name</label>
					        <div class="col-sm-10">
					          <input type="text" class="form-control" id="name" v-model="user.name" required>
					        </div>
					    </div>
					    <div class="form-group row">
					        <label for="inputPassword3" class="col-sm-2 col-form-label">Surname</label>
					        <div class="col-sm-10">
					        <input type="text" class="form-control" id="surname" placeholder="Surname" v-model="user.surname" required>
					        </div>
					    </div>
					    <div class="form-group row">
					        <label for="inputPassword3" class="col-sm-2 col-form-label">Phone number</label>
					        <div class="col-sm-10">
					        <input type="text" class="form-control" id="phone" placeholder="Phone number" v-model="user.phoneNum" required>
					        </div>
					    </div>
					    <div class="form-group row">
					      <div class="col-sm-1">
					        <button type="submit" class="btn btn-primary" @click='update'>Update</button>
					      </div>
					      <div class="col-sm-11"></div>
					      <div class="col-sm-1" style="float: right">
					        <button type="submit" class="btn btn-warning" @click='reset'>Reset</button>
					      </div>
					    </div>
					 </form>
				</div>
			</div>
		</div>
		`,
		methods: {
			update(e) {
				e.preventDefault()
				if (this.admin) {
					axios.post('rest/admin_svc/updateInfo', this.user)
					.then(response => {console.log(response) 
						this.user = response.data})
					.catch(err => console.log(err))
				} else {
					axios.post('rest/user_svc/updateInfo', this.user)
					.then(response => this.user = response.data)
					.catch(err => console.log(err))
				}
			},
			reset() {
				this.user = jQuery.extend(true, {}, this.backup)
			},
			fileUpload() {
				$("#picture").click();
			},
			updatePicture(e) {
	            var files = e.target.files || e.dataTransfer.files;
	            if (!files.length) {
	                return;
	            }
				var file = files[0]
				var ext = files[0].name.split(".").pop()    
	            var headers = {
		                  "Content-Type": "image/*",
		                  "Image-Extension": ext
		                };
	           if (this.admin) {	        	   
	        	   axios.post('rest/admin_svc/uploadPicture', file, { headers: headers })
	        	   .then(response => this.user.picture = response.data)
	        	   .catch(err => console.log(err))
	           } else {
	        	   axios.post('rest/user_svc/uploadPicture', file, { headers: headers })
	        	   .then(response => this.user.picture = response.data)
	        	   .catch(err => console.log(err))
	           }
	        },
		},
		mounted() {
			axios.get('rest/login_svc/getLogged')
			.then(response => {
				this.user = response.data
				this.backup = jQuery.extend(true, {}, this.user)})
			.catch(err => console.log(err)),
			axios.get('rest/login_svc/checkLoginAdmin')
			.then(response => this.admin = response.data)
			.catch(err => console.log(err))
		},
});