Vue.component("users-view", {
	template: `
	<div>
		<div class="row">
			<div class="col-md-1"> </div>
	        <div class="col-md-10">
	            <table class="table">
	                <thead class="thead-dark">
	                    <th scope="col">Username</th>
	                    <th scope="col">Name</th>
	                    <th scope="col">Surname</th>
	                    <th scope="col">Email</th>
	                    <th scope="col">Phone</th>
	                    <th scope="col">Active</th>
	                </thead>
	                <tbody>
						<tr v-for="user in users" v-bind:style= "[!user.active ? {'background-color': '#ff3e3e'} : {}]">
							<td>{{user.username}}</td>
						    <td>{{user.name}}</td>
						    <td>{{user.surname}}</td>
						    <td>{{user.email}}</td>
						    <td>{{user.phoneNum}}</td>
						    <td><input type="checkbox" v-model="user.active" v-on:change="changeActivity(user)"></td>
	    				</tr>
	                </tbody>
	            </table>
	        </div>
	        <div class="col-md-1"> </div>
		</div>
	</div>
	`,
	data() {
		return {			
			users: []
		}
	},
	methods: {
		changeActivity(user) {
			axios.post('rest/user_svc/changeActivity', user)
		}
	},
	mounted() {
		axios.get('rest/user_svc/getUsers')
		.then(response => this.users = response.data.users)
		.catch(err => console.log(err))
	},
});