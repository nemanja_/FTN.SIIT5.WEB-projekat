package beans;

public class Airplane {
	
	private String model;
	private int numFirstClassSeats;
	private int numBusinessClassSeats;
	private int numEconomicClassSeats;
	
	public Airplane() {
		super();
	}
	
	public Airplane(String model, int numFirstClassSeats, int numBusinessClassSeats, int numEconomicClassSeats) {
		super();
		this.model = model;
		this.numFirstClassSeats = numFirstClassSeats;
		this.numBusinessClassSeats = numBusinessClassSeats;
		this.numEconomicClassSeats = numEconomicClassSeats;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getNumFirstClassSeats() {
		return numFirstClassSeats;
	}

	public void setNumFirstClassSeats(int numFirstClassSeats) {
		this.numFirstClassSeats = numFirstClassSeats;
	}

	public int getNumBusinessClassSeats() {
		return numBusinessClassSeats;
	}

	public void setNumBusinessClassSeats(int numBusinessClassSeats) {
		this.numBusinessClassSeats = numBusinessClassSeats;
	}

	public int getNumEconomicClassSeats() {
		return numEconomicClassSeats;
	}

	public void setNumEconomicClassSeats(int numEconomicClassSeats) {
		this.numEconomicClassSeats = numEconomicClassSeats;
	}

}
