package beans;

public class Airport {
	
	private String name;
	private String code;
	private Coordinate coordinate;
	
	public Airport() {
		super();
	}
	
	public Airport(String name, String code, Coordinate coordinate) {
		super();
		this.name = name;
		this.code = code;
		this.coordinate = coordinate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
}
