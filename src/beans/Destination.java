package beans;


public class Destination {

	private String name;
	private String country;
	private Airport airport;
	private boolean active;
	private String picture;
	
	public Destination() {
		super();
	}
	
	public Destination(String name, String country, Airport airport, boolean active, String picture) {
		super();
		this.name = name;
		this.country = country;
		this.airport = airport;
		this.active = active;
		this.setPicture(picture);
	}
	
	public Destination(String name, String country, Airport airport, boolean active) {
		super();
		this.name = name;
		this.country = country;
		this.airport = airport;
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Airport getAirport() {
		return airport;
	}

	public void setAirport(Airport airport) {
		this.airport = airport;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (this.airport.getCode().equals(((Destination)obj).getAirport().getCode()))
			return true;
		
		return false;
	}
		
}
