package beans;

import java.util.Date;

import beans.collections.Reservations;

public class Flight {
	
	private String flightNum;
	private Destination startDestination;
	private Destination endDestination;
	private Reservations reservations;
	private double price;
	private Airplane airplane;
	private Date flightDate;
	private FlightClass flightClass;
	
	public Flight() {
		super();
		this.reservations = new Reservations();
	}
	
	public Flight(String flightNum, Destination startDestination, Destination endDestination,
			Reservations reservations, double price, Airplane airplane, Date flightDate,
			FlightClass flightClass) {
		super();
		this.flightNum = flightNum;
		this.startDestination = startDestination;
		this.endDestination = endDestination;
		this.reservations = reservations;
		this.price = price;
		this.airplane = airplane;
		this.flightDate = flightDate;
		this.flightClass = flightClass;
	}

	public Flight (String flightNum, Destination startDestination, Destination endDestination, double price,
			Airplane airplane, Date flightDate, FlightClass flightClass) {
		super();
		this.flightNum = flightNum;
		this.startDestination = startDestination;
		this.endDestination = endDestination;
		this.reservations = new Reservations();
		this.price = price;
		this.airplane = airplane;
		this.flightDate = flightDate;
		this.flightClass = flightClass;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public Destination getStartDestination() {
		return startDestination;
	}

	public void setStartDestination(Destination startDestination) {
		this.startDestination = startDestination;
	}

	public Destination getEndDestination() {
		return endDestination;
	}

	public void setEndDestination(Destination endDestination) {
		this.endDestination = endDestination;
	}

	public Reservations getReservations() {
		return reservations;
	}

	public void setReservations(Reservations reservations) {
		this.reservations = reservations;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Airplane getAirplane() {
		return airplane;
	}

	public void setAirplane(Airplane airplane) {
		this.airplane = airplane;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public FlightClass getFlightClass() {
		return flightClass;
	}

	public void setFlightClass(FlightClass flightClass) {
		this.flightClass = flightClass;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (this.flightNum.equals(((Flight)obj).getFlightNum()))
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		return "Flight [flightNum=" + flightNum + ", startDestination=" + startDestination + ", endDestination="
				+ endDestination + ", reservations=" + reservations + ", price=" + price + ", airplane=" + airplane
				+ ", flightDate=" + flightDate + ", flightClass=" + flightClass + "]";
	}
	
}
