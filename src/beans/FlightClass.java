package beans;

public enum FlightClass {
	CHARTER,
	REGIONAL,
	TRANSOCEANIC
}
