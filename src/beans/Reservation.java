package beans;

import java.util.Date;

public class Reservation {

	private String id;
	private User user;
	private Date date; 
	private SeatClass seatClass;
	private int passengerNum;
	
	public Reservation() {
		super();
	}
	
	public Reservation(String id, User user, Date date, SeatClass seatClass, int passengerNum) {
		super();
		this.id = id;
		this.user = user;
		this.date = date;
		this.seatClass = seatClass;
		this.passengerNum = passengerNum;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public SeatClass getSeatClass() {
		return seatClass;
	}

	public void setSeatClass(SeatClass seatClass) {
		this.seatClass = seatClass;
	}

	public int getPassengerNum() {
		return passengerNum;
	}

	public void setPassengerNum(int passengerNum) {
		this.passengerNum = passengerNum;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (this.getId().equals(((Reservation)obj).getId()))
				return true;
		
		return false;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", user=" + user + ", date=" + date + ", seatClass=" + seatClass
				+ ", passengerNum=" + passengerNum + "]";
	}
	
}
