package beans.collections;

import java.util.ArrayList;

import beans.Admin;

public class Admins {
	
	private ArrayList<Admin> admins;

	public Admins() {
		super();
		this.admins = new ArrayList<Admin>();
	}

	public Admins(ArrayList<Admin> admins) {
		super();
		this.admins = admins;
	}

	public ArrayList<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(ArrayList<Admin> admins) {
		this.admins = admins;
	}

}
