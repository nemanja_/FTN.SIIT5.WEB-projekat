package beans.collections;

import java.util.ArrayList;

import beans.Destination;

public class Destinations {
	
	private ArrayList<Destination> destinations;

	public Destinations() {
		super();
		this.destinations = new ArrayList<Destination>();
	}

	public Destinations(ArrayList<Destination> destinations) {
		super();
		this.destinations = destinations;
	}

	public ArrayList<Destination> getDestinations() {
		return destinations;
	}

	public void setDestinations(ArrayList<Destination> destinations) {
		this.destinations = destinations;
	}

}
