package beans.collections;

import java.util.ArrayList;

import beans.Flight;

public class Flights {
	
	private ArrayList<Flight> flights;

	public Flights() {
		super();
		this.flights = new ArrayList<Flight>();
	}

	public Flights(ArrayList<Flight> flights) {
		super();
		this.flights = flights;
	}

	public ArrayList<Flight> getFlights() {
		return flights;
	}

	public void setFlights(ArrayList<Flight> flights) {
		this.flights = flights;
	}

}
