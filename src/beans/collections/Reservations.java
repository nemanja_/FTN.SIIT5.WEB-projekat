package beans.collections;

import java.util.ArrayList;

import beans.Reservation;

public class Reservations {
	
	private ArrayList<Reservation> reservations;

	public Reservations() {
		super();
		this.reservations = new ArrayList<Reservation>();
	}

	public Reservations(ArrayList<Reservation> reservations) {
		super();
		this.reservations = reservations;
	}

	public ArrayList<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(ArrayList<Reservation> reservations) {
		this.reservations = reservations;
	}

}
