package beans.collections;

import java.util.ArrayList;

import beans.User;

public class Users {
	
	private ArrayList<User> users;

	public Users() {
		super();
		this.users = new ArrayList<User>();
	}

	public Users(ArrayList<User> users) {
		super();
		this.users = users;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

}
