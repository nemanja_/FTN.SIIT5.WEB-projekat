package services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Admin;
import beans.collections.Admins;

@Path("admin_svc")
public class AdminService {
	
	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/admins")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Admins getUsers() {
		return this.loadAdmins();
	}
	
	@Path("/updateInfo")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Admin updateInfo(Admin a) {
		Admin current = (Admin) request.getSession().getAttribute("admin");
		current.setPassword(a.getPassword());
		current.setEmail(a.getEmail());
		current.setName(a.getName());
		current.setSurname(a.getSurname());
		current.setPhoneNum(a.getPhoneNum());
		
		Admins admins = this.loadAdmins();
		for (Admin admin : admins.getAdmins()) {
			if (admin.getUsername().equals(a.getUsername())) {
				admin.setPassword(a.getPassword());
				admin.setEmail(a.getEmail());
				admin.setName(a.getName());
				admin.setSurname(a.getSurname());
				admin.setPhoneNum(a.getPhoneNum());
				this.saveAdmins(admins);
				return a;
			}
		}
		return null;
	}
	
	@Path("/uploadPicture")
	@POST
	@Consumes("image/*")
	public String uploadFile(InputStream in,
			@HeaderParam("Image-Extension") String extension) {
		
		String fileName = UUID.randomUUID().toString() + "." + extension;
		Admins admins = this.loadAdmins();

		for (Admin admin : admins.getAdmins()) {
			if (admin.getUsername().equals(((Admin) request.getSession().getAttribute("admin")).getUsername())) {
				admin.setPicture(fileName);
				((Admin) request.getSession().getAttribute("admin")).setPicture(fileName);
				break;
			}
		}
		this.saveAdmins(admins);

		java.nio.file.Path BASE_DIR = Paths.get(context.getRealPath(".") + "\\data\\assets");
		try {
			Files.copy(in, BASE_DIR.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			return fileName;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Admins loadAdmins() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Admins admins = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\admins.json"), Admins.class);
			return admins;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAdmins(Admins admins) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\admins.json"), admins);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
