package services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Destination;
import beans.collections.Destinations;

@Path("destinations_svc")
public class DestinationsService {
	
	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/getDestinations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Destinations getDestinations() {
		return this.loadDestinations();
	}
	
	@Path("/addDestination")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Destination addDestination(Destination dest) {
		Destinations destinations = this.loadDestinations();
		if (destinations.getDestinations().contains(dest))
			return null;
		dest.setPicture("destinationDefault.jpg");
		destinations.getDestinations().add(dest);
		this.writeDestinations(destinations);
		return dest;
	}
	
	@Path("/changeDestination")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Destinations changeDestination(Destination dest) {
		Destinations destinations = this.loadDestinations();
		int index = -1;
		for (int i = 0; i < destinations.getDestinations().size(); i++) {
			if (destinations.getDestinations().get(i).equals(dest)) {
				index = i;
				break;
			}
		}
		if (index == -1)
			return null;
		destinations.getDestinations().set(index, dest);
		this.writeDestinations(destinations);
		return destinations;
	}
	
	@Path("/archiveDestination")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean archiveDestination(Destination dest) {
		Destinations destinations = this.loadDestinations();
		for (Destination d : destinations.getDestinations()) {
			if (d.getName().equals(dest.getName()) && d.getCountry().equals(dest.getCountry()) && d.getAirport().getCode().equals(d.getAirport().getCode())) {
				d.setActive(!d.isActive());
				this.writeDestinations(destinations);
				return true;
			}
		}
		return false;
	}
	
	@Path("/uploadPicture")
	@POST
	@Consumes("image/*")
	public String uploadFile(InputStream in,
			@HeaderParam("Image-Extension") String extension,
			@HeaderParam("Destination") String airport_code) {
		String fileName = UUID.randomUUID().toString() + "." + extension;
		Destinations destinations = this.loadDestinations();
		
		for (Destination dest : destinations.getDestinations()) {
			if (dest.getAirport().getCode().equals(airport_code)) {
				dest.setPicture(fileName);
				break;
			}
		}
		this.writeDestinations(destinations);
		
		java.nio.file.Path BASE_DIR = Paths.get(context.getRealPath("."), "\\data\\assets");
		try {
			Files.copy(in, BASE_DIR.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			return fileName;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Destinations loadDestinations() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Destinations destinations = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\destinations.json"), Destinations.class);
			return destinations;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void writeDestinations(Destinations destinations) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\destinations.json"), destinations);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
