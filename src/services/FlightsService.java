package services;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Destination;
import beans.Flight;
import beans.FlightClass;
import beans.SeatClass;
import beans.collections.Destinations;
import beans.collections.Flights;
import beans.collections.Reservations;

@Path("flights_svc")
public class FlightsService {
	
	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/getFlights")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Flights getFlights() {
		return this.loadFlights();
	}
	
	@Path("/getUserFlights")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Flights getUserFlights() {
		Flights loaded = this.loadFlights();
		Flights userFlights = new Flights();
		
		for (Flight f : loaded.getFlights()) {	
			if (f.getStartDestination().isActive() && f.getEndDestination().isActive() && f.getFlightDate().after(new Date())) {
				userFlights.getFlights().add(f);
			}
		}
		return userFlights;
	}
	
	@Path("/getFlightClasses")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public FlightClass[] getFlightClasses() {
		return FlightClass.values();
	}
	
	@Path("/getSeatClasses")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public SeatClass[] getSeatClasses() {
		return SeatClass.values();
	}
	
	@Path("/addFlight")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Flight addFlight(Flight f) {
		if (f.getReservations() == null) {
			f.setReservations(new Reservations());
		}
		Flights flights = this.loadFlights();
		
		if (flights.getFlights().contains(f)) {
			return null;
		}
		
		if (f.getPrice() < 0 ||
			f.getAirplane().getNumBusinessClassSeats() < 0 ||
			f.getAirplane().getNumEconomicClassSeats() < 0 ||
			f.getAirplane().getNumFirstClassSeats() < 0)
			return null;
		
		flights.getFlights().add(f);
		this.writeFlights(flights);
		return f;
	}
	
	@Path("/editFlight")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Flights editFlight(Flight f) {
		Flights flights = this.loadFlights();
		int index = -1;
		for (int i = 0; i < flights.getFlights().size(); i++) {
			if (flights.getFlights().get(i).getFlightNum().equals(f.getFlightNum())) {
				index = i;
				break;
			}
		}
		if (index == -1)
			return null;
		flights.getFlights().set(index, f);
		this.writeFlights(flights);
		return flights;
	}
	
	@Path("/removeFlight")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Flights removeFlight(Flight f) {
		if (f.getReservations().getReservations().size() > 0)
			return null;
		Flights flights = this.loadFlights();
		flights.getFlights().remove(f);
		this.writeFlights(flights);
		return flights;
	}
	
	private Flights loadFlights() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Flights flights = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\flights.json"), Flights.class);
			Destinations destinations = this.loadDestinations();
			
			for (Flight f : flights.getFlights()) {
				for (Destination destination : destinations.getDestinations()) {
					if (destination.equals(f.getStartDestination())) {
						f.setStartDestination(destination);
					}
					if (destination.equals(f.getEndDestination())) {
						f.setEndDestination(destination);
					}
				}
			}			
			return flights;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void writeFlights(Flights flights) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\flights.json"), flights);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Destinations loadDestinations() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Destinations destinations = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\destinations.json"), Destinations.class);
			return destinations;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
