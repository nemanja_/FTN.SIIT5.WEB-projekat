package services;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Admin;
import beans.LoginInfo;
import beans.User;
import beans.collections.Admins;
import beans.collections.Users;

@Path("login_svc")
public class LoginService {

	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String userLogin(LoginInfo info) {
		if (attemptLoginAdmin(info.getUsername(), info.getPassword()))
			return "ADMIN";
		if (attemptLoginUser(info.getUsername(), info.getPassword()))
			return "USER";	
		return null;
	}
	
	@Path("/logout")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public boolean userLogin() {
		request.getSession().invalidate();
		return true;
	}
	
	@Path("/checkLoginAdmin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public boolean checkLoginAdmin() {
		return request.getSession().getAttribute("admin") != null;
	}
	
	@Path("/checkLoginUser")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public boolean checkLoginUser() {
		return request.getSession().getAttribute("user") != null;
	}
	
	@Path("/getLogged")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getLogged() {
		if (request.getSession().getAttribute("admin") != null) {
			return request.getSession().getAttribute("admin");
		}
		if (request.getSession().getAttribute("user") != null) {
			return request.getSession().getAttribute("user");
		}
		return null;
	}
	
	public Users loadUsers() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Users users = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\users.json"), Users.class);
			return users;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Admins loadAdmins() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Admins admins = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\admins.json"), Admins.class);
			return admins;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAdmins(Admins admins) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\admins.json"), admins);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean attemptLoginAdmin(String username, String password) {
		Admins admins = this.loadAdmins();
		for (Admin admin : admins.getAdmins()) {
			if (admin.getUsername().equals(username) && admin.getPassword().equals(password)) {
				request.getSession().setAttribute("admin", admin);
				return true;
			}
		}
		return false;
	}
	
	private boolean attemptLoginUser(String username, String password) {
		Users users = this.loadUsers();
		for (User user : users.getUsers()) {
			if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
				if (user.isActive()) {
					request.getSession().setAttribute("user", user);					
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	public void saveUsers(Users users) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\users.json"), users);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
