package services;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Flight;
import beans.Reservation;
import beans.ReservationInfo;
import beans.User;
import beans.collections.Flights;
import beans.collections.Reservations;

@Path("reservations_svc")
public class ReservationsService {
	
	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/getReservations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Reservations getReservations() {
		Flights flights = this.loadFlights();
		Reservations reservations = new Reservations();
		for (Flight f : flights.getFlights()) {
			for (Reservation r : f.getReservations().getReservations()) {
				if (r.getUser().getUsername().equals(((User)this.request.getSession().getAttribute("user")).getUsername())) {
					reservations.getReservations().add(r);
				}
			}
		}
		return reservations;
	}
	
	@Path("/addReservation")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean addReservation(ReservationInfo reservationInfo) {
		Reservation reservation = reservationInfo.getReservation();		
		if (reservation.getPassengerNum() < 0)
			return false;
		
		Flight flight = reservationInfo.getFlight();
		Flights flights = this.loadFlights();
		
		reservation.setUser((User) this.request.getSession().getAttribute("user"));
		reservation.setId(UUID.randomUUID().toString());
		reservation.setDate(new Date());
		
		for (Flight f : flights.getFlights()) {
			if (f.equals(flight)) {
				if (this.allowReservate(reservation, f)) {
					f.getReservations().getReservations().add(reservation);
					this.writeFlights(flights);
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	@Path("/cancelReservation")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean cancelReservation(Reservation reservation) {
		Flights flights = this.loadFlights();		
		for (Flight flight : flights.getFlights()) {
			if (flight.getReservations().getReservations().contains(reservation)) {
				flight.getReservations().getReservations().remove(reservation);
				this.writeFlights(flights);
				return true;
			}
		}
		return false;
	}
	
	private Flights loadFlights() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Flights destinations = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\flights.json"), Flights.class);
			return destinations;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void writeFlights(Flights flights) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\flights.json"), flights);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean allowReservate(Reservation reservation, Flight flight) {
		int freeSeats = 0;		
		switch (reservation.getSeatClass()) {
			case FIRST: {
				freeSeats = flight.getAirplane().getNumFirstClassSeats();
				break;
			}
			case BUSINESS: {
				freeSeats = flight.getAirplane().getNumBusinessClassSeats();
				break;
			}
			case ECONOMIC: {
				freeSeats = flight.getAirplane().getNumEconomicClassSeats();
				break;
			}
		}
		
		for (Reservation r : flight.getReservations().getReservations()) {
			if (r.getSeatClass().equals(reservation.getSeatClass()))
				freeSeats -= r.getPassengerNum();
		}
		
		return freeSeats >= reservation.getPassengerNum();
	}

}
