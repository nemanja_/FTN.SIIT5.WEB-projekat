package services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.User;
import beans.collections.Users;

@Path("user_svc")
public class UserService {
	
	@Context
	HttpServletRequest request;
	@Context
	ServletContext context;
	
	@Path("/getUsers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Users getUsers() {
		return this.loadUsers();
	}
	
	@Path("/registerUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean registerUser(User user) {
		Users users = this.loadUsers();
		if (users != null) {
			for (User u : users.getUsers()) {
				if (u.getUsername().equals(user.getUsername()))
					return false;
			}
			user.setPicture("userDefault.png");
			users.getUsers().add(user);
			this.saveUsers(users);
			request.getSession().setAttribute("user", user);
			return true;
		} else {
			return false;
		}
	}
	
	@Path("/changeActivity")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean changeActivity(User user) {
		Users users = this.loadUsers();
		for (User registered_user : users.getUsers()) {
			if (registered_user.getUsername().equals(user.getUsername())) {
				registered_user.setActive(!registered_user.isActive());
				this.saveUsers(users);
				return true;
			}
		}
		return false;
	}
	
	@Path("/updateInfo")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User updateInfo(User u) {
		User current = (User) request.getSession().getAttribute("user");
		current.setPassword(u.getPassword());
		current.setEmail(u.getEmail());
		current.setName(u.getName());
		current.setSurname(u.getSurname());
		current.setPhoneNum(u.getPhoneNum());
		
		Users Users = this.loadUsers();
		for (User user : Users.getUsers()) {
			if (user.getUsername().equals(u.getUsername())) {
				user.setPassword(u.getPassword());
				user.setEmail(u.getEmail());
				user.setName(u.getName());
				user.setSurname(u.getSurname());
				user.setPhoneNum(u.getPhoneNum());
				this.saveUsers(Users);
				return user;
			}
		}
		return null;
	}
	
	@Path("/uploadPicture")
	@POST
	@Consumes("image/*")
	public String uploadFile(InputStream in,
			@HeaderParam("Image-Extension") String extension) {
		
		String fileName = UUID.randomUUID().toString() + "." + extension;
		Users users = this.loadUsers();
		
		for (User user : users.getUsers()) {
			if (user.getUsername().equals(((User) request.getSession().getAttribute("user")).getUsername())) {
				user.setPicture(fileName);
				((User) request.getSession().getAttribute("user")).setPicture(fileName);
				break;
			}
		}
		
		java.nio.file.Path BASE_DIR = Paths.get(context.getRealPath("."), "\\data\\assets");
		try {
			Files.copy(in, BASE_DIR.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			this.saveUsers(users);
			return fileName;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Users loadUsers() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Users users = objectMapper.readValue(new File(context.getRealPath(".") + "\\data\\users.json"), Users.class);
			return users;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveUsers(Users users) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(context.getRealPath(".") + "\\data\\users.json"), users);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
